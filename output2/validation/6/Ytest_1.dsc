new("big.matrix.descriptor"
    , description = structure(list(sharedType = "FileBacked", filename = "Ytest_1.bck", 
    totalRows = 84544L, totalCols = 4L, rowOffset = c(0, 84544
    ), colOffset = c(0, 4), nrow = 84544, ncol = 4, rowNames = NULL, 
    colNames = NULL, type = "double", separated = FALSE), .Names = c("sharedType", 
"filename", "totalRows", "totalCols", "rowOffset", "colOffset", 
"nrow", "ncol", "rowNames", "colNames", "type", "separated"))
)
