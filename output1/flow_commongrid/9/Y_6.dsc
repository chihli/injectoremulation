new("big.matrix.descriptor"
    , description = structure(list(sharedType = "FileBacked", filename = "Y_6.bck", 
    totalRows = 70749L, totalCols = 3L, rowOffset = c(0, 70749
    ), colOffset = c(0, 3), nrow = 70749, ncol = 3, rowNames = NULL, 
    colNames = NULL, type = "double", separated = FALSE), .Names = c("sharedType", 
"filename", "totalRows", "totalCols", "rowOffset", "colOffset", 
"nrow", "ncol", "rowNames", "colNames", "type", "separated"))
)
